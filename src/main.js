'use strict';

import Vue from 'vue'
import App from './App.vue'
import VeeValidate from 'vee-validate';
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'

  import datePicker from 'vue-bootstrap-datetimepicker';
  import Select2 from 'v-select2-multiple-component';
  import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
  Vue.use(VeeValidate, {
    events: 'input|blur|dp-hide'
  });
  window.$ = window.jQuery = require('jquery');
require('popper.js');
require('bootstrap');
  Vue.use(datePicker);
Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.component('Select2MultipleControl', Select2);
/*
Vue.directive('toDoStyle',{
  bind(el,binding,vnode){
    if(binding.value=='Done'){
      el.style.borderLeft = '7px solid green';  
    }
    else if(binding.value=='To Do'){
      el.style.borderLeft = '7px solid red';
    }
    else{
      el.style.borderLeft='7px solid yellow';
    }
}
});
*/
new Vue({
  render: h => h(App),
}).$mount('#app')
