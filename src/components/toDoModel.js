export  default class ToDo {
    
    constructor(title, description,start_datetime,end_datetime,comment,status,users){
        this.title=title;
        this.description=description;
        this.start_datetime=start_datetime;
        this.end_datetime=end_datetime;
        this.comment=comment;
        this.status='To Do';
        this.users=users;
    }
    
  }
